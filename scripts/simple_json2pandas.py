import requests
import pandas as pd

r = requests.get('https://jsonplaceholder.typicode.com/todos')
data = r.text
df = pd.read_json(data, orient='records')
print(df)