from fpdf import FPDF


class PDF(FPDF):
    pass

    def titles(self, title):
        self.set_xy(0.0, 0.0)
        self.set_font('helvetica', 'B', 16)
        self.set_text_color(220, 50, 50)
        self.cell(w=210.0, h=40.0, align='C', txt=title, border=0)

    def texts(self, filename):
        with open(filename, 'rb') as file:
            text = file.read().decode('latin-1')
            self.set_xy(10.0, 80.0)
            self.set_text_color(76.0, 32.0, 250.0)
            self.set_font('helvetica', '', 12)
            self.multi_cell(0, 10, text)

    

pdf = PDF()
pdf.add_page()
pdf.image('just_social.png', 0, 0, 60, 60)
pdf.titles('Swiss Just')
pdf.texts('asdfghj.txt')
pdf.set_author('gonzalorivero')
pdf.output('sci.pdf', 'F')
