import uvicorn
from fastapi import FastAPI
from api.routers.scorpy import scorpyRouter

app = FastAPI()
app.include_router(scorpyRouter)

@app.get("/")
def read_root():
    return {"Hello": "World"}

if __name__ == "__main__":
    uvicorn.run(app, host="https://pdfmaker-ten.vercel.app/", port=8001, workers=1)
