# https://pyfpdf.github.io/fpdf2/index.html
# https://github.com/PyFPDF/fpdf2

from fpdf import FPDF
import requests


AUTHOR = "gonzaloriverobaud"
OUTPUT = "main.pdf"


class PDF(FPDF):
    pass

    def create_pdf(self, data):
        self.add_page()
        
        for key in data:
            if key == 'title':
                for title in data[key]:
                    self.set_titles(data[key][title])
            if key == 'image':            
                for image in data[key]:
                    self.set_images(data[key][image])
            if key == 'text':            
                for text in data[key]:
                    self.set_texts(data[key][text])

        self.set_author(AUTHOR)
        self.output(OUTPUT, 'F')

        
    def set_titles(self, title):
        self.set_xy(title['position'][0], title['position'][1])
        self.set_font(title['font'][0], title['font'][1], title['font'][2])
        self.set_text_color(title['color'][0], title['color'][1], title['color'][2])
        self.cell(w=title['cell'][0], h=title['cell'][1], align=title['cell'][2], txt=title['content'], border=title['cell'][3]) 

    def set_images(self, image):
        self.image(image['file'], image['position'][0], image['position'][1], image['size'][0], image['size'][1])

    def set_texts(self, text):
        self.set_xy(text['position'][0], text['position'][1])
        self.set_text_color(text['color'][0], text['color'][1], text['color'][2])
        self.set_font(text['font'][0],text['font'][1], text['font'][2])
        self.multi_cell(text['multi_cell'][0], text['multi_cell'][1], text['content'])
