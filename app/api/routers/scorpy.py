import os
import requests
import ast
from fastapi import Request, APIRouter
from fastapi.responses import FileResponse
from api.api import PDF

scorpyRouter = APIRouter(
    prefix="/scorpy",
    responses={404: {"description": "Not found"}}
)

URL = "https://www.dolarsi.com/api/api.php?type=valoresprincipales"
r = requests.get(URL)

dummy = { 
    "title": {
                0: {
                    "content": "asprin",
                    "position": [0.0, 0.0],
                    "font": ["helvetica", 'B', 16],
                    "color": [220, 50, 50],
                    "cell": [210.0, 40.0, 'C', 0]
                }
    },
    "image": {
                0: {
                    "file":"../static/fpdf2.png",
                    "position": [40.0, 30.0],
                    "size": [60, 60]
                },
                1: {
                    "file":"../static/just_social.png",
                    "position": [100.0, 30.0],
                    "size": [60, 60]
                }
    },
    "text": {
                0: {
                    "content":f"Google Noticias es un agregador y buscador de noticias automatizado {r.json()}",
                    "position": [20.0, 100.0],
                    "font": ["helvetica", 'B', 10],
                    "color": [50, 50, 220],
                    "multi_cell": [0.0, 5.0]
                },
                1: {
                    "content":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    "position": [20.0, 180.0],
                    "font": ["helvetica", 'B', 16],
                    "color": [20, 150, 50],
                    "multi_cell": [80.0, 5.0]
                },
                2: {
                    "content":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    "position": [110.0, 180.0],
                    "font": ["helvetica", 'B', 16],
                    "color": [20, 150, 150],
                    "multi_cell": [80.0, 5.0]
                }
    }
}

@scorpyRouter.get("/get")
async def get_data():
    file_path = os.path.join("main.pdf")
    if os.path.exists(file_path):
        return FileResponse(file_path, media_type="application/pdf")
    return {"error": "File not found!"}

@scorpyRouter.post("/post")
async def post_data(request: Request):
    data = await request.json()
    pdf = PDF()
    pdf.create_pdf(ast.literal_eval(data['value']))

