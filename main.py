# https://pyfpdf.github.io/fpdf2/index.html
# https://github.com/PyFPDF/fpdf2

from fpdf import FPDF
import requests


URL = "https://www.dolarsi.com/api/api.php?type=valoresprincipales"
AUTHOR = "gonzaloriverobaud"
OUTPUT = "main.pdf"

r = requests.get(URL)

data = { 
    "title": {
                0: {
                    "content": "asprin",
                    "position": [0.0, 0.0],
                    "font": ["helvetica", 'B', 16],
                    "color": [220, 50, 50],
                    "cell": [210.0, 40.0, 'C', 0]
                }
    },
    "image": {
                0: {
                    "file":"static/fpdf2.png",
                    "position": [40.0, 30.0],
                    "size": [60, 60]
                },
                1: {
                    "file":"static/just_social.png",
                    "position": [100.0, 30.0],
                    "size": [60, 60]
                }
    },
    "text": {
                0: {
                    "content":f"Google Noticias es un agregador y buscador de noticias automatizado {r.json()}",
                    "position": [20.0, 100.0],
                    "font": ["helvetica", 'B', 10],
                    "color": [50, 50, 220],
                    "multi_cell": [0.0, 5.0]
                },
                1: {
                    "content":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    "position": [20.0, 180.0],
                    "font": ["helvetica", 'B', 16],
                    "color": [20, 150, 50],
                    "multi_cell": [80.0, 5.0]
                },
                2: {
                    "content":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    "position": [110.0, 180.0],
                    "font": ["helvetica", 'B', 16],
                    "color": [20, 150, 150],
                    "multi_cell": [80.0, 5.0]
                }
    }
}

class PDF(FPDF):
    pass

    def create_pdf(self, data):
        self.add_page()
        
        for key in data:
            if key == 'title':
                for title in data[key]:
                    self.set_titles(data[key][title])
            if key == 'image':            
                for image in data[key]:
                    self.set_images(data[key][image])
            if key == 'text':            
                for text in data[key]:
                    self.set_texts(data[key][text])

        self.set_author(AUTHOR)
        self.output(OUTPUT, 'F')

        
    def set_titles(self, title):
        self.set_xy(title['position'][0], title['position'][1])
        self.set_font(title['font'][0], title['font'][1], title['font'][2])
        self.set_text_color(title['color'][0], title['color'][1], title['color'][2])
        self.cell(w=title['cell'][0], h=title['cell'][1], align=title['cell'][2], txt=title['content'], border=title['cell'][3]) 

    def set_images(self, image):
        self.image(image['file'], image['position'][0], image['position'][1], image['size'][0], image['size'][1])

    def set_texts(self, text):
        self.set_xy(text['position'][0], text['position'][1])
        self.set_text_color(text['color'][0], text['color'][1], text['color'][2])
        self.set_font('helvetica', '', 12)
        self.multi_cell(text['multi_cell'][0], text['multi_cell'][1], text['content'])




pdf = PDF()
pdf.create_pdf(data)

